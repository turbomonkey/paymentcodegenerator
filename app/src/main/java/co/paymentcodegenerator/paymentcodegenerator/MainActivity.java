package co.paymentcodegenerator.paymentcodegenerator;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;


public class MainActivity extends ActionBarActivity implements View.OnClickListener {

    private BarcodeEncoder encoder;

    public MainActivity() {
        this.encoder = new BarcodeEncoder();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button = (Button)findViewById(R.id.button);
        button.setOnClickListener(this);

    }

    private void SetBarcodeImage(String message){
        try {

            BitMatrix matrix = encoder.encode(message, BarcodeFormat.QR_CODE, getWallpaperDesiredMinimumWidth(), getWallpaperDesiredMinimumHeight());
            Bitmap bitmap = encoder.createBitmap(matrix);

            // add a bitmap image to the main and then show it.
            ImageView view = (ImageView)findViewById(R.id.imageView);

            view.setImageBitmap(bitmap);

        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        // get the values from the editText

        EditText nameText =  (EditText)findViewById(R.id.editText);
        EditText amountText =  (EditText)findViewById(R.id.editText2);

        String name = nameText.getText().toString();
        String amount = amountText.getText().toString();

        String paymentUrl = String.format("https://paypal.me/%s/%s",name,amount);

        SetBarcodeImage(paymentUrl);
        // hide keyboard

        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
